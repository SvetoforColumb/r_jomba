package com.example.r_jomba

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class MainActivity : AppCompatActivity() {

    lateinit var jokeAdapter: JokeAdapter
    private val jokeApiInterface = JokeApiInterface.create().getJokes()


    fun setJokeList(){
        jokeApiInterface.clone().enqueue(object : Callback<List<Joke>>{ // jokeApiInterface.clone()enqueue() if want to upload parallel and separate
            override fun onResponse(call: Call<List<Joke>>, response: Response<List<Joke>>) {
                if (response.body() != null) {
                    jokeAdapter.setJokesListItems(response.body()!!)
                }
            }

            override fun onFailure(call: Call<List<Joke>>, t: Throwable) {
                Toast.makeText(this@MainActivity, "No jokes for today" , Toast.LENGTH_LONG).show() //todo: move to values
            }

        })
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

//        val toolbar: Toolbar = findViewById<View>(R.id.toolbar) as Toolbar
        itemsswipetorefresh.setColorSchemeColors(resources.getColor(R.color.colorPrimary))


        jokeAdapter = JokeAdapter(this)
        jokesListRecyclerView.layoutManager = LinearLayoutManager(this)
        jokesListRecyclerView.adapter = jokeAdapter

        setJokeList()

        itemsswipetorefresh.setOnRefreshListener {
            setJokeList()
            itemsswipetorefresh.isRefreshing = false
        }


    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.activity_main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean { // Handle item selection
        return when (item.getItemId()) {
            R.id.play_item -> {
                val intent = Intent(this, PlayActivity::class.java)
                this.startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

}
