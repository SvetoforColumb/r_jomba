package com.example.r_jomba

import java.io.Serializable

data class Joke(val id: Int = 0, val type: String = "", val setup: String = "",
                val punchline: String = "") : Serializable