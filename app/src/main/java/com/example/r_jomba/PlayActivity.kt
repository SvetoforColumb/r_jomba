package com.example.r_jomba

import android.content.Context
import android.os.Bundle
import android.view.animation.Animation
import android.view.animation.AnimationUtils
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_play.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import kotlin.random.Random

class PlayActivity : AppCompatActivity() {

    //    var JokeTestList: List<List<Joke>> = listOf()
    lateinit var jokeAdapter: JokeAdapter
    var punchlineButtons: List<Button> = listOf()
    var rightJokeNumber = 0
    var score = 0
    var best_score = 0

    fun getJoke() {
        val jokeApiInterface = JokeApiInterface.create().getJokes()
        jokeApiInterface.enqueue(object : Callback<List<Joke>> { // jokeApiInterface.clone()enqueue() if want to upload parallel and separate
            override fun onResponse(call: Call<List<Joke>>, response: Response<List<Joke>>) {
                if (response.body() != null) {
                    for (button in punchlineButtons){
                        button.setTextColor(resources.getColor(R.color.colorBlack))
                        button.isEnabled = true
                    }
                    punchlineButtons = listOf(punchlineOneButton, punchlineTwoButton, punchlineThreeButton, punchlineFourButton)
                    rightJokeNumber = Random.nextInt(0,3)
//                    Toast.makeText(this@PlayActivity, rightJokeNumber.toString(),Toast.LENGTH_LONG).show() //todo: remove
                    val jokeList = response.body()!!
                    val rightJoke  = jokeList[rightJokeNumber]
                    val fadeIn: Animation = AnimationUtils.loadAnimation(this@PlayActivity, R.anim.fadin)
                    setupTextView.text = rightJoke.setup
                    setupTextView.startAnimation(fadeIn)
                    punchlineButtons[rightJokeNumber].text = rightJoke.punchline
                    for (buttonNumber in punchlineButtons.indices){
                        if (buttonNumber != rightJokeNumber){
                            punchlineButtons[buttonNumber].text = jokeList[buttonNumber].punchline
                        }
                    }
                    for (buttonNumber in punchlineButtons.indices){
                        punchlineButtons[buttonNumber].startAnimation(fadeIn)
                    }
                    for (button_index in punchlineButtons.indices){

                        if (button_index == rightJokeNumber){
                            punchlineButtons[button_index].setOnClickListener {
                                for (button in punchlineButtons){
                                    button.isEnabled = false
                                }
                                score++
                                scoreTextView.text = score.toString()
                                getJoke()
                                scoreTextView.text = "Score: $score"
                                punchlineButtons[button_index].setTextColor(resources.getColor(R.color.colorGreen))
//                                val fadeOut: Animation = AnimationUtils.loadAnimation(this@PlayActivity, R.anim.fadout)
//                                setupTextView.startAnimation(fadeOut)
//                                for (buttonNumber in punchlineButtons.indices){
//                                    punchlineButtons[buttonNumber].startAnimation(fadeOut)
//                                }
                                if (score >= best_score){
                                    best_score = score
                                    bestScoreTextView.text = "Best score: $best_score"
                                    val sharedPref = this@PlayActivity.getPreferences(Context.MODE_PRIVATE)
                                    with (sharedPref.edit()) {
                                        putInt(getString(R.string.preference_best_score), score)
                                        commit()
                                    }
                                }
                            }
                        } else {
                            punchlineButtons[button_index].setOnClickListener {
                                punchlineButtons[button_index].setTextColor(resources.getColor(R.color.colorRed))
                            }
                        }
                    }
                }
            }

            override fun onFailure(call: Call<List<Joke>>, t: Throwable) {
                Toast.makeText(this@PlayActivity, "No setup-punchline game for today", //todo: move to values
                    Toast.LENGTH_LONG).show()
            }

        })
//        val jokeApiInterface = JokeApiInterface.create().getJokes()
//        a = jokeApiInterface.execute().body()!!
    }

//    suspend fun doSomethingUsefulOne(): Int {
//        delay(1000L)
//        return 13
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_play)

        val jokeApiInterface = JokeApiInterface.create().getJokes()
//        val a = jokeApiInterface.execute().body()

        val sharedPref = this@PlayActivity.getPreferences(Context.MODE_PRIVATE)

        best_score = sharedPref.getInt(getString(R.string.preference_best_score), 0)

        scoreTextView.text = "Score: 0"
        bestScoreTextView.text = "Best score: $best_score"

        for (button in punchlineButtons){
            button.isEnabled = false
        }

        getJoke()



    }
}
