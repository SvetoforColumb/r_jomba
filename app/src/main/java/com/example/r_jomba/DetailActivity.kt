package com.example.r_jomba

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.r_jomba.JokeAdapter.JokesViewHolder.Companion.JOKE
import kotlinx.android.synthetic.main.activity_detail.*


class DetailActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)
        val jokeSerialized = intent.getSerializableExtra(JOKE) as Joke
        categoryNameTextView.text = jokeSerialized.type
        val fullJokeText = jokeSerialized.setup + "\n***\n" + jokeSerialized.punchline
        jokeTextView.text = fullJokeText
    }
}
