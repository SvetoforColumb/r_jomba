package com.example.r_jomba

import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.joke_list_item.view.*

class JokeAdapter( val context: Context) : RecyclerView.Adapter<JokeAdapter.JokesViewHolder>() {

    private var jokesList : List<Joke> = listOf()

    override fun getItemCount(): Int {
        return jokesList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): JokesViewHolder {

        val layoutInflater = LayoutInflater.from(context)
        val jokesView = layoutInflater.inflate(R.layout.joke_list_item, parent, false)
        return JokesViewHolder(jokesView)
    }

    override fun onBindViewHolder(holderJokes: JokesViewHolder, position: Int) {
        holderJokes.bind(jokesList[position])
    }

    fun setJokesListItems(jokeList: List<Joke>){
        this.jokesList = jokeList
        notifyDataSetChanged()
    }

    class JokesViewHolder (val view: View) : RecyclerView.ViewHolder(view) {
        private val jokeHeadingTextView: TextView? = view.joke_heading

        fun bind(joke: Joke){
            view.setOnClickListener {
                val intent = Intent(view.context, DetailActivity::class.java)
                intent.putExtra(JOKE, joke)
                view.context.startActivity(intent)
            }
            jokeHeadingTextView?.text = joke.setup
        }

        companion object {
            const val JOKE = "joke_id"
        }
    }

}




