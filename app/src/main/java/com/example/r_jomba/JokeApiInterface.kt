package com.example.r_jomba

import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET


interface JokeApiInterface {

    @GET("random_ten")
    fun getJokes() : Call<List<Joke>>



    companion object {
        var BASE_URL = "https://official-joke-api.appspot.com/"

        fun create(): JokeApiInterface {
            val retrofit = Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl(BASE_URL)
                .build()
            return retrofit.create(JokeApiInterface::class.java)
        }

//        fun getApi(): JokeApiInterface {
//            return jokeApiInterface
//        }
    }
}